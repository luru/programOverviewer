# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 11:17:46 2018

@author: luru
"""

from pythonModules import osTools as ost
import os, sys, requests
import matplotlib.pyplot as plt
import numpy as np
import argparse
import scipy.io as sio

debug=False

#,{'port':'AEF50','TM':'TM3h','finger':5}
kwargs  = {'diagnostics':['QRP','QIR'], 
           'details':{
                   'QRP':['ne','Te'],		   
                   'QIR':[{'port':'AEF51','TM':'TM3h','finger':5,'clipval':4}] #must be list to work for ne, Te, but is otherwise stupid this way, doesnt't work with replot
                   },
           'info':['ECRH','LID'], 
           'replot':True,
           'divertor':'UD',
		   'title':False,
		   'subtitles':False,
		   'fontsize':16,
		   #nasty, should be program generic
		   'timesOfInterest':[[2.5,'pre-detachment','b'],[3.1,'power detachment','r'],[3.7,'partial power detachment','g']]
           }

#kwargs['details']['QIR'].pop(1)
#program = '20171207.029'
program = sys.argv[1]
day=program[:-4]
experiment=program[-3:]

data={}
IRtimes,IRvalues,LPtimes,ECRHt,ECRH, Te,ne,IRpos = np.zeros(8)

'''
#To properly parse kwargs from the command line more work is needed
args=argparse.ArgumentParser()
for key in kwargs.keys():
    args.add_argument(
            key,nargs="*")

for i,a in enumerate(sys.argv):
    print('arg'+str(i)+' : '+a)

for key in kwargs.keys():
    if key in sys.argv:
        kwargs[key]=sys.argv[sys.argv.index(key)+1]
'''
paths=[]

def getBaseFolderPath(diagnostic):
    if diagnostic == 'QRP':
        return 'X:\E4 Diagnostics\QRP_Langmuir_Sonden\OP1.2_TDU-Sonden\Preliminary_Analysis'
    if diagnostic == 'QIR':
        return 'X:\E4 Diagnostics\QIR\Data_Analysis'

def getProgramFolderPath(diagnostic):
    if diagnostic == 'QRP':
        return day
    if diagnostic == 'QIR':
        return day+'\\'+program

def getDetailedName(diagnostic, detail):
	if diagnostic == 'QRP':
		return detail+'_'+experiment+'.png'
	if diagnostic == 'QIR':
		return detail['port']+'_'+program+'_'+detail['TM']+'_'+str(detail['finger'])+'.png'

def loadData(diagnostic, detail):
	reusltsFolder = getBaseFolderPath(diagnostic)+'\\'+getProgramFolderPath(diagnostic)+'\\'
	if diagnostic == 'QRP':
		if detail == 'times':
			data[detail]= np.loadtxt(reusltsFolder+detail+str(program[-3:]+'.txt'))
		else: 
			data[detail]= np.loadtxt(reusltsFolder+kwargs['divertor']+'_'+detail+str(program[-3:]+'.txt'))
		#Te    = np.loadtxt(reusltsFolder+kwargs['divertor']+'_Te'+str(program[-3:]+'.txt'))
		#Te    = np.clip(Te,0,100)
		#ne    = np.loadtxt(reusltsFolder+kwargs['divertor']+'_ne'+str(program[-3:]+'.txt'))
	if diagnostic == 'QIR':
		IRmat = sio.loadmat(reusltsFolder+detail['port']+'_'+program)
		IRvalues = IRmat['heatflux_'+detail['TM']+'_'+str(detail['finger'])]
		clipval=kwargs['details']['QIR'][0]['clipval'] #MW/m{^2} at which theodor calculation is cut
		if clipval<np.max(IRvalues):print('max heat flow is '+str(np.max(IRvalues))+' display is clipped at '+str(clipval))
		data['IRvalues'] = np.clip(IRvalues,0,clipval)
		if debug: 
			print('IRvalues path is '  + reusltsFolder+detail['port']+'_'+program)
			print('IRvalues shape is ' + str(np.shape(IRvalues)))
			print('IRvalues type is '  + str(type(IRvalues)))
		data['IRtimes'] = IRmat['time']
		IRpos = IRmat['location_'+detail['TM']+'_'+str(detail['finger'])][0]
		data['IRpos'] = np.abs(IRpos-max(IRpos))		

def getTrigger(t):
    url = 'http://archive-webapi.ipp-hgw.mpg.de/programs.json'
    res = requests.get(url, params={'from': program}).json()
    try:
        r=res['programs'][0]['trigger'][str(t)][0]
    except Exception as e:
        print(str(type(e))+' Error')
        r=None
    return r
    
def plotECRH():
	preT1offset=100000000 #100ms
	url = 'http://archive-webapi.ipp-hgw.mpg.de/ArchiveDB/codac/W7X/CBG_ECRH/TotalPower_DATASTREAM/V2/0/Ptot_ECRH/_signal.json'
	res = requests.get(url, params={'from': getTrigger(1)-preT1offset,'upto':getTrigger(5)}).json()
	ECRHt=(np.asarray(res['dimensions'])-getTrigger(1))/1E9
	ECRHv=np.asarray(res['values'])/1E3
	if kwargs['replot']: return ECRHt,ECRHv
	fig=plt.figure()
	ax=fig.add_subplot(111)
	ax.plot(ECRHt,ECRHv)
	ax.set_xlabel('Time from t1 in s')
	ax.set_ylabel('ECRH Power in MW')
	title='Total ECRH power for program '+program
	ax.set_title(title)
	fig.canvas.set_window_title(title)


def plotLineDensity():
	preT1offset=100000000 #100ms
	url = 'http://archive-webapi.ipp-hgw.mpg.de/ArchiveDB/codac/W7X/CoDaStationDesc.16339/DataModuleDesc.16341_DATASTREAM/0/Line%20integrated%20density/_signal.json'
	res = requests.get(url, params={'from': getTrigger(1)-preT1offset,'upto':getTrigger(5)}).json()
	LIDt=(np.asarray(res['dimensions'])-getTrigger(1))/1E9
	LIDv=np.asarray(res['values'])
	if kwargs['replot']: return LIDt,LIDv
	fig=plt.figure()
	ax=fig.add_subplot(111)
	ax.plot(LIDt,LIDv)
	ax.set_xlabel('Time from t1 in s')
	ax.set_ylabel('Line integrated density in $m^{-2}$')
	title='Line integrated density for program '+program
	ax.set_title(title)
	fig.canvas.set_window_title(title)
	
	
def processInfo():
	info = kwargs['info']
	if 'ECRH' in info:
		plotECRH()
	if 'LID' in info:
		plotLineDensity()
    
def assemblePaths():
	for diag in kwargs['diagnostics']:
		for det in kwargs['details'][diag]:
			paths.append(getBaseFolderPath(diag)+'\\'+getProgramFolderPath(diag)+'\\'+getDetailedName(diag,det))

def assembleData():
	for diag in kwargs['diagnostics']:
		for det in kwargs['details'][diag]:
			loadData(diag,det)

			
def printPaths():    
    for p in paths:
        print(p)

if not kwargs['replot']:
	#Open images at paths
	assemblePaths()
	if debug: printPaths()
	ost.process(paths, ['']*len(paths), maxProcesses=len(paths)+1)
	print('Images loaded')
	processInfo()
	plt.show()



if kwargs['replot']:
	#plot as one
	assembleData()
	plt.rcParams.update({'mathtext.default': 'regular','font.size':16})

	fig=plt.figure(figsize=(8,10))
	ax1=fig.add_subplot(411)
	ax2=fig.add_subplot(412,sharex=ax1)
	ax3=fig.add_subplot(413,sharex=ax1)
	ax4=fig.add_subplot(414,sharex=ax1)
	ax5=ax4.twinx()
	
	IRx, IRy = np.meshgrid(data['IRtimes'],data['IRpos'])
	if debug: 
		#print('IRvalues path is '  + reusltsFolder+detail['port']+'_'+program)
		print('IRvalues shape is ' + str(np.shape(data['IRvalues'])))
		print('IRvalues type is '  + str(type(data['IRvalues'])))
		print(str(np.shape(IRx))+' '+str(np.shape(IRy))+' '+str(np.shape(data['IRvalues'].T)))
	IRim=ax1.contourf(IRx,IRy,data['IRvalues'].T)

	LPpos = [p/1000 for p in [421.40,396.43 ,371.48 ,346.55 ,321.62 , 246.94 ,222.10 ,197.26 ,172.52 ,147.85 ]] #are these specific to one divertor?
	for p in LPpos:
		ax1.axhline(y=p,lw=1,c='r')
		ax1.set_ylim(0,max(LPpos)+0.02)

	loadData('QRP','times')

	LPx, LPy = np.meshgrid(data['times'],LPpos)
	Teim = ax2.contourf(LPx, LPy, np.clip(data['Te'],0,100))
	neim = ax3.contourf(LPx, LPy, data['ne'])

	ax4.plot(*plotECRH())
	ax5.plot(*plotLineDensity(),'g-')
	
	clipval=kwargs['details']['QIR'][0]['clipval']
	#cb1=fig.colorbar(IRim,ax=ax1,ticks=[0,clipval/2,clipval],label='$Heat\/ load\/ [MW/m{^2}]$')
	#cb1=fig.colorbar(IRim,ax=ax1,ticks=[0,1,2,3,4],label='$Heat\/ flux\/ [MW/m{^2}]$')
	cb1=fig.colorbar(IRim,ax=ax1,ticks=[0,1,2,3,4],label='$Infrared\ q\ [MW/m{^2}]$')
	cb2=fig.colorbar(Teim,ax=ax2,ticks=[0,20,40,60,80,100],label='$Langmuir\ T_e\ [eV]$')
	#cb3=fig.colorbar(neim,ax=ax3,ticks=[0,3e18,6e18,9e18,12e18],format='%2.e',label='$n_e\/ [m^{-3}]$')
	cb3=fig.colorbar(neim,ax=ax3,ticks=[0,3e18,6e18,9e18,12e18],label='$Langmuir\ n_e\ [m^{-3}]$')
	cb4=fig.colorbar(neim,ax=ax4)
	cb5=fig.colorbar(neim,ax=ax5)
	cb4.remove()
	cb5.remove()

	if kwargs['title']: fig.suptitle(kwargs['divertor']+' '+str(program))
	ax1.set_xlim(-0.1,4.4) #for all plots
	

	plt.setp( ax1.get_xticklabels(), visible=False)
	plt.setp( ax2.get_xticklabels(), visible=False)
	plt.setp( ax3.get_xticklabels(), visible=False)

	fig.text(0.06, 0.66, 'Distance from pumping gap [m]', va='center', ha='center', rotation='vertical',fontsize=16)
	ax4.set_ylabel('ECRH Power [MW]',fontsize=kwargs['fontsize'])
	ax4.set_xlabel('Time from ECRH start [s]',fontsize=kwargs['fontsize'])
	ax5.set_ylabel('Interferometer\n$\int\/ n_e\/ [m^{-2}]$',fontsize=kwargs['fontsize'])
	plt.subplots_adjust(hspace=0.15,right=0.9,top=0.95)
	
	if kwargs['subtitles']:
		ax1.set_title('Infrared',fontsize=kwargs['fontsize'])
		ax2.set_title('Langmuir',fontsize=kwargs['fontsize'])
		ax3.set_title('Langmuir',fontsize=kwargs['fontsize'])
		ax4.set_title('ECRH and Interferometer',fontsize=kwargs['fontsize'])
	
	if kwargs['timesOfInterest']:
		for t,l,c in kwargs['timesOfInterest']:
			ax1.axvline(x=t,lw=3,c=c)
			ax2.axvline(x=t,lw=3,c=c)
			ax3.axvline(x=t,lw=3,c=c)
			ax4.axvline(x=t,lw=3,c=c)
	bbox_props = dict(boxstyle="darrow,pad=0.3", fc="w",ec='k', lw=1)
	t = ax4.text(2.7, 0.5, "H2 puff", ha="left", va="bottom", size=10, bbox=bbox_props)
	plt.show()
	#plt.savefig('output.png')
